# Vorlage für Bachelor-/Masterarbeit

Autor: 		Nicolas Peschke
Version: 	2.2
Datum:		09.07.2018

Die Vorlage kann unter Studenten der Molekularen Biotechnologie der	Universität Heidelberg weitergegeben und verwendet werden.

Für die Richtigkeit und Konformität des Layouts mit den Richtlinien des IPMBs wird keine Haftung übernommen, **BITTE SPRECHT AUF JEDEN FALL DAS LAYOUT EURER ARBEIT MIT EUREN BETREUERN AB!!!**

Um die Titelseite und die zweite Seite richtig zu setzen müsst ihr nur in dieser Datei unter "Definitionen für Titelseite" die auf euch passenden Eintragungen vornehmen.	In der Datei declaration.tex muss dann noch die Anschrift der	Prüfer richtig angegeben werden.
Für das Zitieren muss im Befehl `\addbibresource{bib/bachelorarbeit.bib}` durch den Dateinamen und Dateipfad eurer BibTeX Datei aus Citavi an.
